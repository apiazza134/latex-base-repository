import os
import sys
import shutil
from yaml import safe_load

CONF = sys.argv[1]

with open(CONF, 'r') as f:
    conf = safe_load(f)

if conf['license']['present']:
    files = [f"{conf['license']['type']}.eps", 'gitattributes']
    for f in files:
        shutil.copy(os.path.join('license', f), 'build')

for filename in os.listdir('build'):
    if filename[:3] == 'git':
        os.rename(
            os.path.join('build', filename),
            os.path.join('build', f'.{filename}')
        )
