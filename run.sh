#!/usr/bin/env bash

set -e

function usage () {
    cat <<EOF
$(basename $0) [OPTIONS]

Options:
  -f, --file TEXT       configuration file, skips the interactive cli

Interactive cli help
$(python3 conf.py --help)
EOF
    exit 0
}

while [ "$1" != "" ]; do
    case $1 in
        -f | --file ) shift
                      filename=$1;;
        -h | --help ) usage
                      exit;;
    esac
    shift
done

CONF_DEFAULT_NAME="conf.yml"
if ! test -f "${filename}"; then
    if test -f ${CONF_DEFAULT_NAME}; then
        echo "# Configuration file conf.yml already exixst: skipping interactive cli"
    else
        echo "# Interactive cli to create project configuration file"
        python3 conf.py
    fi
    filename=${CONF_DEFAULT_NAME}
fi

echo "# Rendering templates and creating git repository"
python3 render.py ${filename}
python3 create.py ${filename}
cd build && git init && cd ..
