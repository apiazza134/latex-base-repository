![build status](https://gitlab.com/apiazza134/latex-base-repository/badges/master/pipeline.svg)

# Tool for creating basic LaTeX repositories

## Description
This tool aims to provide an *agile* way to create a basic git repository containing for latex project with continuous GitLab integration. It was born from the desire to automate the process of creating git project for university notes.

The final output is a folder containing the following files
- a bare `main.tex` with can contain (according to your preferences) a license disclaimer [creative-commons](https://creativecommons.org/)
- `style.sty` which provides some common LaTeX packages and macros that I use almost everywhere
- `.gitignore` based on [gitignore.io](https://www.toptal.com/developers/gitignore/api/latex)
- a `.eps` file containing a license badge (if desired)
- `.gitattributes` to ignore the `.eps` file in the language toolbar of the GitLab repository
- `.gitlab-ci.yml` which provides continuous integration for the latex project and exports the `.pdf` file produced as artifacts
- `README.md` with the status of the pipeline and the link to the most recent `.pdf` produced by the CI

## Use
Clone the repository and install the required python packages in the `requirements.txt` file. I recommend using a virtual environment:
```bash
virtualenv -p python3 venv
source venv / bin / activ
pip install -r requirements.txt
```

Then you just have to execute the `run.sh` script which will call some python scripts. You will be prompted to a command line interface to fill in some data necessary for rendering some templates. This will produce a `.yml` configuration file: an example of the output is provided by` conf.yml.example`.

In the `build` directory you will find all the above mentioned files filled with the data provided in the configuration file. The script already launches a `git init` inside this directory.

You can skip the interactive CLI by directly providing the `conf.yml` file with
```bash
./run.sh -f conf.yml
```

Project creation on GitLab has not yet to be implemented.
