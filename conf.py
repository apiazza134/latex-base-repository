#!/usr/bin/env python3

import click
from configparser import ConfigParser
import os
from yaml import safe_load, dump

# TODO: validazione degli input


class OptionSlug(click.Option):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_default(self, ctx):
        return (ctx.params['repo_name']).lower().replace(' ', '-')


def get_git_user():
    config = ConfigParser()
    config.read(f"/home/{os.environ.get('USER')}/.gitconfig")
    try:
        return config['user']['name']
    except KeyError:
        return None


def validate_no_spaces(ctx, param, value):
    if value.find(' ') != -1:
        raise click.BadParameter('whitespaces not allowed!')
    else:
        return value


with open('default.yml', 'r') as f:
    default = safe_load(f)


@click.command()
@click.option('--repo_owner',
              prompt='Repository owner', type=str, default=get_git_user,
              callback=validate_no_spaces,
              help='Repository owner.')
@click.option('--repo_name',
              prompt='Repository name', type=str,
              help='Repository name.')
@click.option('--repo_slug',
              cls=OptionSlug,
              prompt='Repository slug', type=str,
              callback=validate_no_spaces,
              help='Repository slug.')
@click.option('--repo_visibility',
              prompt='Repository visibility on GitLab', type=click.Choice(['private', 'public']), default=default['repo']['visibility'],
              help='Repository visibility on GitLab.')
@click.option('--ci_docker_image',
              prompt='CI docker image', type=str, default=default['ci']['docker_image'],
              callback=validate_no_spaces,
              help='Docker image to use in GitLab CI.')
@click.option('--ci_file_name',
              prompt='CI output file name (without the extension .pdf)', type=str,
              callback=validate_no_spaces,
              default=default['ci']['output_pdf_name'],
              help='Final pdf file name.')
@click.option('--license_present',
              prompt='Add license to tex file?', type=bool, default=default['license']['present'],
              help='Add license to tex file.')
@click.option('--license_type',
              prompt='Choose license type', type=click.Choice(['by', 'by-nc', 'by-nc-nd', 'by-nc-sa', 'by-nd', 'by-sa']), default=default['license']['type'],
              help='Creative-commons license type (ignored if license_present is false)')
@click.confirmation_option(prompt='Is this information correct?')
def run(**kwargs):
    conf = {
        'repo': {
            'owner': kwargs['repo_owner'],
            'name': kwargs['repo_name'],
            'slug': kwargs['repo_slug'],
            'visibility': kwargs['repo_visibility']
        },
        'ci': {
            'docker_image': kwargs['ci_docker_image'],
            'file_name': kwargs['ci_file_name']
        },
        'license': {
            'present': kwargs['license_present'],
            'type': kwargs['license_type']
        }
    }
    with open('conf.yml', 'w') as yaml_file:
        dump(conf, yaml_file)


if __name__ == '__main__':
    run()
