![build status](https://gitlab.com/{{ repo.owner }}/{{ repo.slug }}/badges/master/pipeline.svg)

# {{ repo.name }}

Il PDF più recente si può scaricare [qui](https://gitlab.com/{{ repo.owner }}/{{ repo.slug }}/-/jobs/artifacts/master/raw/{{ ci.file_name }}.pdf?job=compile_pdf)
