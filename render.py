import os
import sys
from shutil import rmtree

from yaml import safe_load as yaml_safe_load
from jinja2 import Environment, FileSystemLoader

CONF = sys.argv[1]
TEMPLATE_DIR = 'templates'
OUTPUT_DIR = 'build'
LATEX_EXTENSIONS = ['.tex', '.sty']

LICENSES = {
    'by': 'Attribuzione',
    'sa': 'Condividi allo stesso modo',
    'nd': 'Non opere derivate',
    'nc': 'Non commerciale'
}

basic_env = Environment(
    loader=FileSystemLoader(TEMPLATE_DIR)
)
latex_env = Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=FileSystemLoader(TEMPLATE_DIR)
)

if os.path.exists(OUTPUT_DIR):
    rmtree(OUTPUT_DIR, ignore_errors=True)
os.makedirs(OUTPUT_DIR)

with open(CONF, 'r') as f:
    conf = yaml_safe_load(f)

conf['license']['description'] = ' - '.join(
    [
        LICENSES[tag] for tag in (conf['license']['type']).split('-')
    ]
)

templates = [f for f in os.listdir(TEMPLATE_DIR) if os.path.isfile(os.path.join(TEMPLATE_DIR, f))]
for filename in templates:
    env = latex_env if os.path.splitext(filename)[1] in LATEX_EXTENSIONS \
        else basic_env
    template = env.get_template(filename)
    with open(os.path.join(OUTPUT_DIR, filename), 'w') as out:
        out.write(
            template.render(conf)
        )
